<?php
/*
Template Name: Home
*/

get_header('single'); ?>

		<div id="content" class="site-content container row clearfix clear">
	<div class="container col-md-12">
	
<div id="primary-main" class="content-area col-md-12">
		<main id="main" class="site-main row container" role="main">
		<?php 
			$query = array('post_type' => 'post', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DES');
			$category_posts = new WP_Query($query);
	    ?>
		<?php if ( $category_posts->have_posts() ) : ?>

			<?php /* Start the Loop */ $ink_count = 0; $ink_row_count=0 ?>
			<?php while ( $category_posts->have_posts() ) : $category_posts->the_post(); 
				if ( has_post_thumbnail() ) {
					if ($ink_count == 0 ) {echo "<div class='row-".$ink_row_count." row'>";}
				?>
				

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', 'home' );
					?>

				<?php 
					if ($ink_count == 2 )
						{
							echo "</div>";
							$ink_count=0;
							$ink_row_count++;
						}
					else {	
						$ink_count++;
					}
				}
				endwhile; 
			?>
			<?php fifteen_pagination(); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary-main -->

<?php get_footer(); ?>